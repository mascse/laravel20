<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Services\DemoOne;

use Helper;

class TestController extends Controller
{
    public function helper(){
		$aa  = Helper::shout('this is how to use autoloading correctly!!');
		echo $aa;exit;
		
	}
    public function index(DemoOne $customServiceInstance)
    {
        echo $customServiceInstance->doSomethingUseful();
    }

}
